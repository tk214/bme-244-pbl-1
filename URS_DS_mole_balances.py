import numpy as np

state = "healthy"

#Default parameters:
FRC = 4690
IRV = 3000
ERV = 1100
TV = (FRC-IRV-ERV)
    

if(state == "diseased1"):
    TV = TV*1.02/1.02
elif(state == "diseased2"):
    TV = TV*0.72/1.02

BR = 16
    
# n = PV/RT     where R = 62.364
n_1_total = 760*(BR*TV/1000)/(62.364*(37+273.15))
P_1_O2 = 0.1967
P_1_CO2 = 0.0004
n_1_O2 = n_1_total * P_1_O2
m_1_O2 = n_1_O2*(2*15.999)
n_1_CO2 = n_1_total * P_1_CO2
m_1_CO2 = n_1_CO2*(12.011+2*15.999)

n_2_total = n_1_total
P_2_O2 = 0.1570
P_2_CO2 = 0.0360
n_2_O2 = n_2_total * P_2_O2
m_2_O2 = n_2_O2*(2*15.999)
n_2_CO2 = n_2_total * P_2_CO2
m_2_CO2 = n_2_CO2*(12.011+2*15.999)

n_5_O2 = n_1_O2 - n_2_O2
m_5_O2 = n_5_O2*(2*15.999)
n_6_CO2 = n_2_CO2 - n_1_CO2
m_6_CO2 = n_6_CO2*(12.011+2*15.999)


print("5: {}, 6: {}".format(n_5_O2,n_6_CO2))