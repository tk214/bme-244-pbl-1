#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 12:20:41 2023

@author: samanthamiller
"""

import numpy  as np
import matplotlib.pyplot as plt

# Import functions that we designed that will be useful
from gastracker import *

#%% FUNCTIONS
# create_data() -- this function initializes a data structure and starting values
def create_data():
    run_data = {} # Initialize run_data as a dict where each key is a unit
    run_data["cb"] = {
            "O2" : [0.000552755905512],
            "CO2" : [0.000122834645669],
            "nutrients" : [0],
            "waste" : [0],
            "bodymass" : [0]
        }
    run_data["sm"] = {
            "O2" : [0.000552755905512*(3.1993)/.078],
            "CO2" : [0.000122834645669*(3.1993)/.078],
            "nutrients" : [0],
            "waste" : [0],
            "bodymass" : [0]
        }
    run_data["em"] = {
            "O2" : [0.000552755905512*(1.7227)/.078],
            "CO2" : [0.000122834645669*(1.7227)/.078],
            "nutrients" : [0],
            "waste" : [0],
            "bodymass" : [0]
        }
    run_data["fa"] = {
            "O2" : [0],
            "CO2" : [0],
            "nutrients" : [0],
            "waste" : [0],
            "bodymass" : [0]
        }
    run_data["urs"] = {
            "O2" : [0],
            "CO2" : [0],
            "nutrients" : [0],
            "waste" : [0],
            "bodymass" : [0]
        }
    run_data["frac"] = [0.35] # frac key stores ALL fractional splits of Qt
    run_data["O2diff_em"] = [0] # Steady state determiner
    run_data["O2diff_sm"] = [0] # Steady state determiner
    return run_data

#%% Test 1 -- O2 lack vs. different levels of COPD
alv_surf_losses = np.linspace(0,1,100)
em_diff = []
sm_diff = []
artCO2pp = []
artO2pp = []
dt = .001
ss_tol = .001

for COPDf in alv_surf_losses:
    test_data = create_data() # initialize a new data struc. each time
    ss_counter = 0
    i = 1
    while ss_counter < 50:
        # Grab data
        nO2_em = test_data["em"]["O2"][i-1]
        nO2_sm = test_data["sm"]["O2"][i-1]
        nO2_cb = test_data["cb"]["O2"][i-1]
        nCO2_em = test_data["em"]["CO2"][i-1]
        nCO2_sm = test_data["sm"]["CO2"][i-1]
        nCO2_cb = test_data["cb"]["CO2"][i-1]
        current_frac = test_data["frac"][i-1]

        # Perform the update step
        gasupdate = meta_gas_step(nO2_em, nO2_sm, nO2_cb, nCO2_em, nCO2_sm, nCO2_cb, prev_frac=current_frac, COPDk=COPDf)

        # Update values in dictionary
        test_data["em"]["O2"].append(nO2_em+gasupdate[0]*dt)
        test_data["sm"]["O2"].append(nO2_sm+gasupdate[1]*dt)
        test_data["cb"]["O2"].append(nO2_cb+gasupdate[2]*dt)
        test_data["em"]["CO2"].append(nO2_em+gasupdate[3]*dt)
        test_data["sm"]["CO2"].append(nO2_sm+gasupdate[4]*dt)
        test_data["cb"]["CO2"].append(nO2_cb+gasupdate[5]*dt)
        test_data["frac"].append(gasupdate[6])
        test_data["O2diff_em"].append(25.4*1000*gasupdate[7][0])
        test_data["O2diff_sm"].append(25.4*1000*gasupdate[7][1])

        # Determine if steady state
        if abs(test_data["O2diff_em"][i] - test_data["O2diff_em"][i-1]) < ss_tol and abs(test_data["O2diff_sm"][i] - test_data["O2diff_sm"][i-1]) < ss_tol:
            ss_counter += 1
        
        i += 1
    em_diff.append(-test_data["O2diff_em"][-1])
    sm_diff.append(-test_data["O2diff_sm"][-1])
    artCO2pp.append(numtoppCO2(test_data["cb"]["CO2"][-1],.078))
    artO2pp.append(numtoppO2(test_data["cb"]["O2"][-1],.078))

COPDfig = plt.figure(num=1, clear=True)
SMlossfig = plt.figure(num=2, clear=True)
ppartfig = plt.figure(num=3, clear=True)
jCO2ppfig = plt.figure(num=4, clear=True)

ax = COPDfig.add_subplot(1,1,1)
ax1 = SMlossfig.add_subplot(1,1,1)
ax2 = ppartfig.add_subplot(1,1,1)
ax3 = jCO2ppfig.add_subplot(1,1,1)
ax.set(xlabel="% of alveolar surface destroyed", ylabel="O2 diffusion (mL O$_2$ / min)")
ax1.set(xlabel="% of alveolar surface destroyed", ylabel="% of O$_2$ diffusion in SM lost")
ax2.set(xlabel="% of alveolar surface destroyed", ylabel="gas partial pressure (mmHg)")
ax3.set(xlabel="% of alveolar surface destroyed", ylabel="CO$_2$ partial pressure (mmHg)")
ax.plot([100*i for i in alv_surf_losses], em_diff, label="EM O$_2$ diffusion")
ax.plot([100*i for i in alv_surf_losses], sm_diff, label="SM O$_2$ diffusion")
ax.plot([100*i for i in alv_surf_losses], [em_diff[i]+sm_diff[i] for i in range(len(em_diff))], label="Total O$_2$ diffusion")
ax1.plot([100*i for i in alv_surf_losses], [100-100*sm_diff[i]/(245*.65) for i in range(len(sm_diff))])
ax2.plot([100*i for i in alv_surf_losses], artCO2pp, label="Arterial ppCO2")
ax2.plot([100*i for i in alv_surf_losses], artO2pp, label="Arterial ppO2")
ax3.plot([100*i for i in alv_surf_losses], artCO2pp, label="ppCO2 in CB")
ax.legend()
ax1.legend()
ax2.legend()
ax3.legend()
COPDfig.savefig("images/COPDgraph.png")
SMlossfig.savefig("images/COPDlackgraph.png")
ppartfig.savefig("images/artPPs.png")
jCO2ppfig.savefig("images/CO2pp.png")