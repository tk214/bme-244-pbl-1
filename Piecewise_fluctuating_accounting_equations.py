import numpy as np
import matplotlib.pyplot as plt

state = "healthy"

#Default parameters:
FVC = 4690
IRV = 3000
ERV = 1100
TV = (FVC-IRV-ERV)
    

if(state == "diseased1"):
    TV = TV*1.02/1.02
elif(state == "diseased2"):
    TV = TV*0.72/1.02


def tide(TV,time):
    tide = TV*np.sin(16*np.pi*time/60)**2
    return(tide)

def delta_tide(TV,time):
    delta_tide = 2*TV*np.sin(16*np.pi*time/60)*np.cos(16*np.pi*time/60)*(16*np.pi/60)
    return(delta_tide)

def moles_in_out(tide,time):
    if(0<= time % 3.75 <= 1.875):
        n_1_total = 760*(tide(TV,time)/1000)/(62.364*(37+273.15))
        P_1_O2 = 0.1967
        P_1_CO2 = 0.0004
        n_1_O2 = n_1_total * P_1_O2
        m_1_O2 = n_1_O2*(2*15.999)
        n_1_CO2 = n_1_total * P_1_CO2
        m_1_CO2 = n_1_CO2*(12.011+2*15.999)
        P_2_O2 = 0.1570
        P_2_CO2 = 0.0360
        n_5_O2 = (P_1_O2-P_2_O2)*n_1_total
        n_6_CO2 = (P_1_CO2-P_2_CO2)*n_1_total
    else:
        n_1_total = 760*(tide(TV,time)/1000)/(62.364*(37+273.15))
        P_1_O2 = 0.1967
        P_1_CO2 = 0.0004
        n_1_O2 = n_1_total * P_1_O2
        m_1_O2 = n_1_O2*(2*15.999)
        n_1_CO2 = n_1_total * P_1_CO2
        m_1_CO2 = n_1_CO2*(12.011+2*15.999)
        P_2_O2 = 0.1570
        P_2_CO2 = 0.0360
        n_5_O2 = 0
        n_6_CO2 = 0
    return(n_1_total,n_1_O2,n_1_CO2,n_5_O2,n_6_CO2)

def delta_moles(delta_tide,time):
    if(0<= time % 3.75 <= 1.875):
        delta_n_1_total = 760*(delta_tide(TV,time)/1000)/(62.364*(37+273.15))
        P_1_O2 = 0.1967
        P_1_CO2 = 0.0004
        delta_n_1_O2 = delta_n_1_total * P_1_O2
        delta_m_1_O2 = delta_n_1_O2*(2*15.999)
        delta_n_1_CO2 = delta_n_1_total * P_1_CO2
        delta_m_1_CO2 = delta_n_1_CO2*(12.011+2*15.999)
        P_2_O2 = 0.1570
        P_2_CO2 = 0.0360
        delta_n_5_O2 = (P_1_O2-P_2_O2)*delta_n_1_total
        delta_n_6_CO2 = (P_1_CO2-P_2_CO2)*delta_n_1_total
    else:
        delta_n_1_total = 760*(delta_tide(TV,time)/1000)/(62.364*(37+273.15))
        P_1_O2 = 0.1967
        P_1_CO2 = 0.0004
        delta_n_1_O2 = delta_n_1_total * P_1_O2
        delta_m_1_O2 = delta_n_1_O2*(2*15.999)
        delta_n_1_CO2 = delta_n_1_total * P_1_CO2
        delta_m_1_CO2 = delta_n_1_CO2*(12.011+2*15.999)
        P_2_O2 = 0.1570
        P_2_CO2 = 0.0360
        delta_n_5_O2 = 0
        delta_n_6_CO2 = 0
    return(delta_n_1_total,delta_n_1_O2,delta_n_1_CO2,delta_n_5_O2,delta_n_6_CO2)


time = np.arange(0,60,0.01)
n_5_O2 = np.zeros(len(time))
delta_n_5_O2 = np.zeros(len(time))
n_6_CO2 = np.zeros(len(time))
delta_n_6_CO2 = np.zeros(len(time))
for x in range(len(time)):
    currentTime = time[x]
    n_5_O2[x] = moles_in_out(tide,currentTime)[3]
    delta_n_5_O2[x] = delta_moles(delta_tide,currentTime)[3]
    n_6_CO2[x] = moles_in_out(tide,currentTime)[4]
    delta_n_6_CO2[x] = delta_moles(delta_tide,currentTime)[4]

fig = plt.figure(num=1, clear=True)
ax1 = fig.add_subplot(2,1,1)
ax2 = fig.add_subplot(2,1,2)
ax1.plot(time,n_5_O2,label='Stream 5 ($O_2$)')
ax1.plot(time,n_6_CO2,label='Stream 6 ($CO_2$)')
ax2.plot(time,delta_n_5_O2,label='Rate of Stream 5 ($O_2$)')
ax2.plot(time,delta_n_6_CO2,label='Rate of Stream 6 ($CO_2$)')
ax1.grid()
ax1.legend()
fig.tight_layout()
ax2.grid()
ax2.legend()
