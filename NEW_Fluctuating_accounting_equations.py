import numpy as np
import matplotlib.pyplot as plt

state = "healthy"

#Default parameters:
FVC = 4690
IRV = 3000
ERV = 1100
TV = (FVC-IRV-ERV)
    

if(state == "diseased1"):
    TV = TV*1.02/1.02
elif(state == "diseased2"):
    TV = TV*0.72/1.02


def tide(TV,time):
    tide = TV*np.sin(16*np.pi*time/60)**2
    return(tide)

def delta_tide(TV,time):
    delta_tide = 2*TV*np.sin(16*np.pi*time/60)*np.cos(16*np.pi*time/60)*(16*np.pi/60)
    return(delta_tide)

def moles_in_out(tide,time):
    n_1_total = 760*(tide(TV,time)/1000)/(62.364*(37+273.15))
    P_1_O2 = 0.1967
    P_1_CO2 = 0.0004
    n_1_O2 = n_1_total * P_1_O2
    m_1_O2 = n_1_O2*(2*15.999)
    n_1_CO2 = n_1_total * P_1_CO2
    m_1_CO2 = n_1_CO2*(12.011+2*15.999)
    P_2_O2 = 0.1570
    P_2_CO2 = 0.0360
    n_5_O2 = (P_1_O2-P_2_O2)*n_1_total
    n_6_CO2 = (P_1_CO2-P_2_CO2)*n_1_total
    return(n_1_total,n_1_O2,n_1_CO2,n_5_O2,n_6_CO2)

def delta_moles_in_out(delta_tide,time):
    delta_n_1_total = 760*(delta_tide(TV,time)/1000)/(62.364*(37+273.15))
    P_1_O2 = 0.1967
    P_1_CO2 = 0.0004
    delta_n_1_O2 = delta_n_1_total * P_1_O2
    delta_m_1_O2 = delta_n_1_O2*(2*15.999)
    delta_n_1_CO2 = delta_n_1_total * P_1_CO2
    delta_m_1_CO2 = delta_n_1_CO2*(12.011+2*15.999)
    P_2_O2 = 0.1570
    P_2_CO2 = 0.0360
    delta_n_5_O2 = (P_1_O2-P_2_O2)*delta_n_1_total
    delta_n_6_CO2 = (P_1_CO2-P_2_CO2)*delta_n_1_total
    return(delta_n_1_total,delta_n_1_O2,delta_n_1_CO2,delta_n_5_O2,delta_n_6_CO2)


time = np.arange(0,60,0.01)
fig = plt.figure(num=1, clear=True)
ax = fig.add_subplot(1,1,1)
#ax.plot(time,(moles_in_out(tide,time))[0],label='Total moles in/out')
ax.plot(time,(delta_moles_in_out(delta_tide,time))[3],label='dStream 5 ($O_2$)')
ax.plot(time,(delta_moles_in_out(delta_tide,time))[4],label='dStream 6 ($CO_2$)')
#ax.plot(time,(moles_in_out(tide,time))[3],label='Stream 5 ($O_2$)')
#ax.plot(time,(moles_in_out(tide,time))[4],label='Stream 6 ($CO_2$)')
ax.grid()
ax.set(xlabel="Arterial $CO_{2}$ Partial Pressure (mmHg)",ylabel="Average Volume (mL)")
ax.legend()
fig.show()
fig.tight_layout()
