"""
metsplit: function that splits stream 14 (exiting capillary bed) into streams 9 (entering skeletal muscle) and 10 (entering bulk cell metabolism) based on the available oxygen and the oxygen demand of the bulk cell metabolism.

TK 2/26/23
Updated TK 3/1/23
"""
import numpy as np
import matplotlib.pyplot as plt
from fickdiffusion import *

O2split = .35 # the brain and heart demand ~35% of the O2 in the body, this is the % of the total metabolism that is essential
critO2rate = 0.245/25.4*O2split # mol/min of O2 needed to supply the essential metabolism

# metsplit() is the updated metabolic splitter
# nO2_em is the moles of O2 in the essential metabolism unit, Vem is the volume of the essential metabolism unit
# prev_frac is the previous % of blood into the essential metabolism, defaulting to O2split = 35%
# the initial if statement limits the CO % to only the brain, heart, and lungs
def metsplit(nO2_em, prev_frac=.35, Vem=1.7227):
    if prev_frac < .70:
        resp_em = -0.35*(0.245/(25.4*31))*(40 - numtoppO2(nO2_em, Vem)) # neg cause this function returns a negative number & we need +
        ess_met = 0.35*(0.245/25.4)
        supply_frac = resp_em/ess_met # fraction of essential metabolic demand met
        if supply_frac < 1:
            Qscale = 1/supply_frac # arbitrary scaling factor the body implements to restore metabolism
        else:
            return prev_frac
    else:
        return prev_frac
    return Qscale*prev_frac

#%% mbsplit() is the old metabolic splitter
def mbsplit(n_cbO2, Qt=5.5, Vcb=.087):
    O2avail = Qt*(n_cbO2/Vcb)*(2*15.999) # rate of O2 in g/min
    if O2avail >= critO2rate:
        return O2split
        # if there's enough O2 in the body, just divide normally
    elif O2avail >= critO2rate:
        return critO2mass / O2avail
        # if there's insufficient O2, divide so the BCM gets the critO2mass
    else:
        return 1
        # if there's less than critO2mass in stream 14, give all to BCM     

#%% Code to produce graph showing results
"""
testO2s = np.linspace(0.0001,.00011,100) # List of O2 mass tests
testsplits = [100*mbsplit(testO2s[i]) for i in range(len(testO2s))] # get ratios

O2testfig = plt.figure(num=1,clear=True)
ax = O2testfig.add_subplot(1,1,1)
ax.set(
     xlabel="O2 to body (mdot_14,O2) (g/min)",
     ylabel="percent blood volume to BCM"
)

# Plot the data
ax.plot(testO2s,testsplits,'k-')

plt.tight_layout()
O2testfig.savefig("images/O2Splits.png")
"""