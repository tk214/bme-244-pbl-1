#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 12:20:41 2023

@author: samanthamiller
"""

import numpy as np
import matplotlib.pyplot as plt

## EQUATIONS

dx= 1
x = np.zeros(525600)
for k in range(0,525600):
    x[k] = dx*k
m1o = np.zeros(15) 
m1o[0] = 1.98


for i in range(0,525600):
    m1o[i+1] = 1.98*m1o[i]/1.98
    
plt.plot(x,m1o)
plt.xlim()
plt.show()
 
    
m1o=1.98 * x
m1c= .0553*x
m2o= 1.58*x
m2c= .498*x
m5o= .397*x
m6c= .442*x
m3 = .33*x
m7= m5o
m8= m6c
m14o = m7 - .097

if m14o.any() >= .15:
    m10o = m14o /2
    m9o = m14o /2
else:
    m10o = m14o
    m9o = m14o-m10o
    
m11c=m9o
m12c=m10o
m11w=-.00454*x -.33*x
m13w = m11w
m4 = m3 + m13w
m13c = m12c +m11c


#print(m7)
#print(m8)
#print(m14o)
#print(m10o)
#print(m9o)
#print(m11c)
#print(m12c)
#print(m13w)
#print(m11w)
#print(m4)
#print(m13w)

x = sym.Symbol('x')
dm1o = sym.integrate(m1o,x)
plot()

# plotting the points 
plt.plot(dm1o)
plt.plot(x,m4)

plt.xlabel('Days')
plt.ylabel('Weight Loss (kg)')
  
plt.show()
# Use space most effectively


