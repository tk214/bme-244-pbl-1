# BME 244 PBL 1

## Repository structure
- The "PBL1.py" file should contain what we RUN to get model results
- Functions for making graphics, splitting streams, calculating pressures, etc. should be located OUTSIDE of this file and should be imported / called as necessary
- If generating plots, etc. export them into the images directory (so then everyone can see them!)

## Metabolism notes
- Metabolic units include bulk cell metabolism (BCM), skeletal muscle (SM), and the capillary bed (CB)
- The whole metabolic system only has in/out streams at CB, which are 3 (nutrient/IN), 4 (waste/OUT), 7 (O2/IN), and 8 (CO2/OUT)
- The stream out of the CB (14) splits into streams 9 (SM/IN) and 10 (BCM/IN) according to the function developed in `metabolicsplitter.py`
- Streams 11 (SM/OUT) and 12 (BCM/OUT) add simply to get stream 13 (CB/IN)
- The mass rate value of stream 3 is completely dependent on the rates of metabolism in the BCM and SM units; i.e. all nutrients are consumed by the metabolism (no nutrients in stream 11, 12, or 13 at all time t) 
- No explicit function to get nutrient or waste, but we can use the respiration rate and equation (1 C6H12O6 + 6 O2 --> 6 CO2 + 6 H20) to conduct a full mass balance on the system

## FORMATTING STREAM DATA
We should format all stream and compartment data in the following manner:

run_data = {
    "stream_1" :
    {
        "CO2" : [],
        "O2" : [],
        "nutrients" : [],
        "waste" : [],
        "muscle" : []
    }
    "stream_2" :
    {
        "CO2" : [],
        "O2" : [],
        "nutrients" : [],
        "waste" : [],
        "muscle" : []
    }
    .
    .
    .
}

i.e., the data contained in a run of the model "run_data" is a dictionary where the keys are the 14 streams, and each stream is a dictionary where the keys are the components, and each component corresponds to an array.

- May not need the 14 streams, actually, unless we want to track ... will think/work on