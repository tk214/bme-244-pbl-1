"""
fickflux: function that calculates the diffusive flux between the alveolar gas and alveolar capillaries for O2 and CO2
numtoppO2 and numtoppCO2: function that converts num moles & compartment volume to partial pressures

TK 2/26/23
"""

import numpy as np
import matplotlib.pyplot as plt

# Guyton and Hall list that O2 diffusion in a *healthy young man* is 21 mL O2 / mmHg & also list that CO2 has a difussion coefficient 20 times (400-450 mL CO2 / mmHg); thus for O2 diffusion, D = 21 mmO2/mmHg, and for CO2 diffusion, D = 425 mLCO2/mmHg
# the mol parameter MUST be either "CO2" or "O2"
# for now, we are pretending that the amt of gas exchange from the alveolar air into the capillaries is small relative to the total volume so we can hold both ppEXTs as constant; later these will be modeled as function w/ the respiratory cycle
# COPDpct is the % alveoli destroyed in emphysema
# n_capbed will be passed in as a NUMBER that is the number of MOLES GAS
# returns mL gas / min

def fickflux(mol, n_capbed, COPDpct=0):
    if mol == "CO2":
        D = 425
        ppEXT = 40 #mmHg, Guyton and Hall
        ppINT = numtoppCO2(n_capbed, .078)
    elif mol == "O2":
        D = 21
        ppEXT = 104 #mmHG, Guyton and Hall
        ppINT = numtoppO2(n_capbed, .078)
    else:
        return 0
    return D*(ppEXT-ppINT)*(1-COPDpct)

# Referencing Sander, kH[s] is in mol/(m^3*Pa); convert to mol/(L*mmHg)
# T is physiological temperature in Kelvin
# Only unbound O2 contributes partial pressure; healthy people have ~ 97.5% O2 bound (is func of O2 conc. but can add later)
# 10% of CO2 is bound to hemoglobin (?)

def numtoppO2(no, vol, T=310.15):
    khO2 = (1.25*10**-5)*np.e**(1650*(1/298.15-1/T))*(133.322/1000) # Temperature dependent Herny's coeff
    conc = no*(1-.975)/vol # 2.5% as free O2 in blood
    pp = conc/khO2
    return pp

def numtoppCO2(no, vol,T=310.15):
    khCO2 = (3.35*10**-4)*np.e**(2400*(1/298.15-1/T))*(133.322/1000) # Temperature dependent Henry's coeff
    conc = no*(1-.93)/vol # 7% as free CO2 in blood
    pp = conc/khCO2
    return pp

#%% Testing to see if the code produces results that make sense.
"""
capbedtestO2 = np.linspace(.012,.017,1000)
capbedtestCO2 = np.linspace(.005,.010,1000)

testdiffO2s = [fickflux("O2",capbedtestO2[i]/22.4) for i in range(len(capbedtestO2))]
testdiffCO2s = [fickflux("CO2",capbedtestCO2[i]/22.4) for i in range(len(capbedtestCO2))]

O2testfig = plt.figure(num=1,clear=True)
CO2testfig = plt.figure(num=2,clear=True)

ax1 = O2testfig.add_subplot(1,1,1)
ax1.set(
    xlabel="(mL O2 / 100 mL) in capillary bed",
    ylabel="O2 flux (mL O2/min)"
)

ax2 = CO2testfig.add_subplot(1,1,1)
ax2.set(
    xlabel="(mL CO2 / 100 mL) in capillary bed",
    ylabel="CO2 flux (mL CO2/min)"
)

# Plot the data
ax1.plot(1000*.100/.078*capbedtestO2,testdiffO2s,'k-')
ax2.plot(1000*.100/.078*capbedtestCO2,testdiffCO2s,'k-')

plt.tight_layout()
O2testfig.savefig("images/O2diffusiontest.png")
CO2testfig.savefig("images/COdiffusiontest.png")
"""