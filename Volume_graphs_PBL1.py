import matplotlib.pyplot as plt
import numpy as np

#Initialize variables
v_t = 500.
v_frc = 3000.
P_aCO2 = 40.
P_eCO2 = 27.
fraction = (P_aCO2-P_eCO2)/P_aCO2
state = "healthy" #healthy or diseased


if(state == "diseased"):
    P_eCO2 = 0.065+ 0.88*(1.38 + 0.64 * P_aCO2)

def v_tot(t):
    v_tot = v_t * np.sin(16*np.pi*t/60)**2 + v_frc
    return v_tot

def v_d(t):
    v_d = fraction*v_tot(t)
    return v_d

time = np.arange(0,25,0.01)

fig = plt.figure(num=1, clear=True)
ax = fig.add_subplot(1,1,1)
ax.plot(time,v_tot(time),label='Total Lung Volume')
ax.plot(time,v_d(time),label='Physiological Dead Space Volume')
ax.plot(time,v_tot(time)-v_d(time),label='Functional Alveolar Volume')
ax.grid()
ax.set(xlabel="seconds",ylabel="mL")
ax.legend()
fig.tight_layout()


#Calculating average total volumes
avg_total = np.mean(v_tot(time))
avg_deadspace = np.mean(v_d(time))
avg_functional = np.mean(v_tot(time)-v_d(time))

print("Average total volume: {:.3f} \nAverage dead space volume: {:.3f} \nAverage functional alveolar volume: {:.3f}".format(avg_total,avg_deadspace,avg_functional))