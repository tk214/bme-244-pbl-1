#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  2 10:21:53 2023

@author: samanthamiller
"""

import numpy  as np
import matplotlib.pyplot as plt

#g_o2 = np.linspace(0, .2005, 100)
t = np.linspace(0,365, 1000)


def weightlossrates(g_o2):
    if(g_o2>0.047726080445436546):
        return 12.128 -(139.87 * (.2005-g_o2)) + (395.91* ((.2005-g_o2)**2))
    else:
        return 0


g_o2 = np.linspace(0, .2005, 1000)

yaxis = np.zeros(len(g_o2))

for x in range(len(g_o2)):
    yaxis[x] = weightlossrates(g_o2[x]) 

print(weightlossrates(.15))
closs1 = 70 -t * .00744
closs2 = 70- t * .00376
closs3 = 70 - t * .001552
clossh = 70 -t * .0000000000001

y = np.linspace(-2,14,4)
healthy_x = np.array([0.2005-0.198495,0.2005-0.198495,0.2005-0.198495,0.2005-0.198495])
d2_x = np.array([0.2005-0.086215,0.2005-0.086215,0.2005-0.086215,0.2005-0.086215])
d1_x = np.array([0.2005-0.162405,0.2005-0.162405,0.2005-0.162405,0.2005-0.162405])


plt.figure(1)
plt.plot(g_o2 ,yaxis)
plt.plot(healthy_x,y,'--',label='Healthy State')
plt.plot(d1_x,y,'--',label='Mild Disease State')
plt.plot(d2_x,y,'--',label='Severe Disease State')

plt.xlim()
plt.title("Weight Loss Rate vs $O_2$ Consumption")
plt.xlabel("Loss of $O_2$ (g/min)")
plt.ylabel("Rate of weight loss (kg/year)")
plt.show()
plt.legend()
plt.grid()



plt.figure(2)
plt.plot(t, closs1)
plt.plot(t, closs2)
plt.plot(t, closs3)
plt.plot(t, clossh)
plt.xlim()
plt.title("Time v.s weight lost")
plt.xlabel("Time (days")
plt.ylabel("Weight lost (kg)")
plt.show()
