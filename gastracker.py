"""
Functions to track constituent accumulation and movement over time in the metabolic units
All functions are a d/dt step derived from various differential accounting equations of the general form:
    d/dt(acc) = d/dt(in-out+gen-con) = d/dt(in-out+sR)

TK 2/28/23
"""

# Load necessary modules and functions
import numpy as np
import matplotlib.pyplot as plt
from metabolicsplitter import *
from fickdiffusion import *

# For each of these, pass in the entire run_data object
# Qt = cardiac output, 5.5 L/min
# Vem = 1.7227 L, Vsm = 3.1993 L, Vcb = .078 L

# d/dt step for O2 or CO2 in the bulk cell metabolism, skeletal muscle, and capillary bed
# the six required inputs are the data at the last time step (ns_em, ns_sm, and ns_cb) for CO2 and O2
# COPDk is the percent alveoli dead, prev_frac is the previous fraction used to split the cardiac output
# meta_gas_step returns a vector of length eight containing all of the new gas numbers, the CO fraction, and a vector containing respiration data (used as a diagnostic)

def meta_gas_step(nO2_em, nO2_sm, nO2_cb, nCO2_em, nCO2_sm, nCO2_cb, prev_frac=.35, COPDk=0, Qt=5.5, Vem=1.7227, Vsm=3.1993, Vcb=.078):
    frac = metsplit(nO2_em, prev_frac) # models vasodilation/constriction mechanism to divert more blood to em if needed
    resp_em = 0.35*.245/(31*25.4)*(40-numtoppO2(nO2_em, Vem)) # How much diffusion occurs; don't use frac here b/c is a func. of BODY MASS
    resp_sm = 0.65*.245/(31*25.4)*(40-numtoppO2(nO2_sm, Vsm))

    # O2 differentials
    dnO2_em = -Qt*frac*(nO2_em/Vem) + Qt*frac*(nO2_cb/Vcb) + resp_em
    dnO2_sm = -Qt*(1-frac)*(nO2_sm/Vsm) + Qt*(1-frac)*(nO2_cb/Vcb) + resp_sm
    dnO2_cb = -Qt*(nO2_cb/Vcb) + Qt*frac*(nO2_em/Vem) + Qt*(1-frac)*(nO2_sm/Vsm) + fickflux("O2", nO2_cb, COPDpct=COPDk)/(1000*25.4)

    # CO2 differentials
    dnCO2_em = -Qt*frac*(nCO2_em/Vem) + Qt*frac*(nCO2_cb/Vcb) - resp_em # resp negative because opposite of O2
    dnCO2_sm = -Qt*(1-frac)*(nCO2_sm/Vsm) + Qt*(1-frac)*(nCO2_cb/Vcb) - resp_sm
    dnCO2_cb = -Qt*(nCO2_cb/Vcb) + Qt*frac*(nCO2_em/Vem) + Qt*(1-frac)*(nCO2_sm/Vsm) + fickflux("CO2", nCO2_cb, COPDpct=COPDk)/(1000*25.4)

    return [dnO2_em, dnO2_sm, dnO2_cb, dnCO2_em, dnCO2_sm, dnCO2_cb, frac, [resp_em, resp_sm]]

#%% Deprecated version of the gas step; NOT UPDATED for EM/SM redesign
def meta_gas_step_old(mol, run_data, Qt=5.5, Vbcm=2.9532, Vsm=1.6988, Vcb=.078):
    ns_bcm = run_data["bcm"][mol][-1] # no of moles gas in BCM at last time step
    ns_sm = run_data["sm"][mol][-1] # no of moles gas in SM at last time step
    ns_cb = run_data["cb"][mol][-1] # no of moles gas in CB at last time step
    frac = mbsplit(run_data["cb"]["O2"][-1]) # determines how to split blood stream (14)

    if mol == "O2":
        resp_bcm = frac*.245/(31*25.4)*(40-numtoppO2(run_data["bcm"]["O2"][-1], Vbcm)) # How much diffusion occurs
        resp_sm = (1-frac)*.245/(31*25.4)*(40-numtoppO2(run_data["sm"]["O2"][-1], Vsm)) # How much diffusion occurs
    elif mol == "CO2":
        resp_bcm = - frac*.245/(31*25.4)*(40-numtoppO2(run_data["bcm"]["O2"][-1], Vbcm))
        resp_sm = - (1-frac)*.245/(31*25.4)*(40-numtoppO2(run_data["sm"]["O2"][-1], Vsm))
    else:
        resp = 0

    dn_bcm = -Qt*frac*(ns_bcm/Vbcm) + Qt*frac*(ns_cb/Vcb) + resp_bcm
    dn_sm = -Qt*(1-frac)*(ns_sm/Vsm) + Qt*(1-frac)*(ns_cb/Vcb) + resp_sm
    dn_cb = -Qt*(ns_cb/Vcb) + Qt*frac*(ns_bcm/Vbcm) + Qt*(1-frac)*(ns_sm/Vsm) + fickflux(mol, ns_cb,COPDpct=0)/(1000*25.4) # convert from mL/min to mol/min

    return [dn_bcm, dn_sm, dn_cb, [resp_bcm, resp_sm]] # final element is diagnostic

#%% Testing Gas Method
"""
print(5,5,5)

# quick data setup w/ predetermined initial concentrations
test_data = {
    "em" : {"O2" : [0.000552755905512*(1.7227)/.078], "CO2" : [0.000122834645669*(1.7227)/.078]},
    "sm" : {"O2" : [0.000552755905512*(3.1993)/.078], "CO2" : [0.000122834645669*(3.1993)/.078]},
    "cb" : {"O2" : [0.000552755905512], "CO2" : [0.000122834645669]},
    "frac" : [.35]
}

# track diffusions as diagnostic
O2diff_emdata = []
O2diff_smdata = []

# use an Euler method because that's easy
dt = .001
for i in range(1,100000):
    nO2_em = test_data["em"]["O2"][i-1]
    nO2_sm = test_data["sm"]["O2"][i-1]
    nO2_cb = test_data["cb"]["O2"][i-1]
    nCO2_em = test_data["em"]["CO2"][i-1]
    nCO2_sm = test_data["sm"]["CO2"][i-1]
    nCO2_cb = test_data["cb"]["CO2"][i-1]
    current_frac = test_data["frac"][i-1]

    gasupdate = meta_gas_step(nO2_em, nO2_sm, nO2_cb, nCO2_em, nCO2_sm, nCO2_cb, prev_frac=current_frac, COPDk=0.5)

    # UPDATE
    test_data["em"]["O2"].append(nO2_em+gasupdate[0]*dt)
    test_data["sm"]["O2"].append(nO2_sm+gasupdate[1]*dt)
    test_data["cb"]["O2"].append(nO2_cb+gasupdate[2]*dt)
    test_data["em"]["CO2"].append(nO2_em+gasupdate[3]*dt)
    test_data["sm"]["CO2"].append(nO2_sm+gasupdate[4]*dt)
    test_data["cb"]["CO2"].append(nO2_cb+gasupdate[5]*dt)
    test_data["frac"].append(gasupdate[6])
    O2diff_emdata.append(25.4*1000*gasupdate[7][0])
    O2diff_smdata.append(25.4*1000*gasupdate[7][1])

O2testfig = plt.figure(num=1,clear=True)
CO2testfig = plt.figure(num=2,clear=True)
CBtestfig = plt.figure(num=3,clear=True)
difftestfig = plt.figure(num=4,clear=True)
fracfig = plt.figure(num=5,clear=True)

ax1 = O2testfig.add_subplot(1,1,1)
ax1.set(xlabel="t (min)", ylabel="mol O2 in various locations)")
ax2 = CO2testfig.add_subplot(1,1,1)
ax2.set(xlabel="t (min)", ylabel="mol CO2 in various locations")
ax3 = CBtestfig.add_subplot(1,1,1)
ax3.set(xlabel="t (min)", ylabel="mol gas in CB")
ax4 = difftestfig.add_subplot(1,1,1)
ax4.set(xlabel="t (min)", ylabel="mL O2/min diffused into cells")
ax5 = fracfig.add_subplot(1,1,1)
ax5.set(xlabel="t (min)", ylabel="frac CO into EM")

# Plot the data
times = np.linspace(0,100.001,100000)
ax1.plot(times, test_data["sm"]["O2"],'b-', label="O2 in SM")
ax1.plot(times, test_data["em"]["O2"],'k-', label="O2 in EM")
ax2.plot(times, test_data["sm"]["CO2"],'b-', label="CO2 in SM")
ax2.plot(times, test_data["em"]["CO2"],'k-', label="CO2 in EM")
ax3.plot(times, test_data["cb"]["O2"],'b-', label="O2 in CB")
ax3.plot(times, test_data["cb"]["CO2"],'k-', label="CO2 in CB")
ax4.plot(times[1:],O2diff_emdata,'b-', label="em")
ax4.plot(times[1:],O2diff_smdata,'k-', label="sm")
ax5.plot(times, test_data["frac"])

plt.tight_layout()
ax1.legend()
ax2.legend()
ax3.legend()
ax4.legend()
ax5.legend()

O2testfig.savefig("images/O2cycletest.png")
CO2testfig.savefig("images/CO2cycletest.png")    
CBtestfig.savefig("images/CBcycletest.png")
difftestfig.savefig("images/cellO2diffusion.png")
fracfig.savefig("images/COfrac.png")
"""